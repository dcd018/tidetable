from setuptools import setup

setup(
    name='TideTable',
    version='0.1dev',
    packages=['tidetable'],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.md').read(),
)
