import tidetable
import json

if __name__ == '__main__':
    import sys
    locations = len(sys.argv) >= 2 and sys.argv[1].split(',') or [
        'Half-Moon-Bay-California',
        'Providence-Rhode-Island',
        'Wrightsville-Beach-North-Carolina'
    ]
    print json.dumps(tidetable.parse(locations), indent=2, sort_keys=True)