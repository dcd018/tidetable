# tidetable

An HTMLParser written in Python providing a basic utility for extracting tide information from [tide-forecast.com](https://www.tide-forecast.com).

## Running Locally

This was tested with v2.7.12, make sure you have the correct version of Python installed if you experience any issues.

```sh
$ git clone git@bitbucket.org:dcd018/tidetable.git # or clone your own fork
$ python tidetable
```
Or specify location(s) as the third argument

```sh
$ python tidetable Half-Moon-Bay-California,Providence-Rhode-Island
```
Executing this package as a main program without arguments will parse tide information for default locations and output as a JSON formatted string. See the `__main__.py` file for importing and usage in another script or module.

```python
import tidetable
import json

print json.dumps(tidetable.parse([
    'Half-Moon-Bay-California',
    'Providence-Rhode-Island',
    'Wrightsville-Beach-North-Carolina'
]), indent=2, sort_keys=True)
```

You can also use a Python interpereter to quickly test a location

```sh
$ python
Python 2.7.12 (default, Dec  4 2017, 14:50:18) 
[GCC 5.4.0 20160609] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import tidetable
>>> data = tidetable.parse(['Half-Moon-Bay-California'])
>>> print data
```
