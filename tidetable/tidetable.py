from HTMLParser import HTMLParser

import urllib2

class TideTable(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        
        self.capture = 0
        self.section = 0
        self.td = 0
        self.tr = 0

        self.table = {}
        self.data = []

    def __getitem__(self, name):
        return getattr(self, name)
    
    def __setitem__(self, name, value):
        return setattr(self, name, value)

    def handle_starttag(self, tag, attributes):
        if attributes:
            name, value = attributes[0]
            if tag == 'section' and name == 'class' and 'tide-events' in value:
                self.section = value
                return
            if self.section and tag in ['tr', 'td']:
                if tag == 'tr' and self.tr and value != self.tr:
                    self.data.append(self.table)
                    self.table = {}
                self[tag] = value


    def handle_endtag(self, tag):
        if tag == 'td' and self.td:
            self.td = 0

    def handle_data(self, data):
        if data in ['Sunrise', 'Sunset']:
            self.capture = 1 if data == 'Sunrise' else 0
            return 
        if self.td == 'date' or self.td and self.capture and data:
            if self.td not in self.table:
                self.table[self.td] = []
            self.table[self.td].append(data)

def assign(location):
    url = 'https://www.tide-forecast.com/locations/'+location+'/tides/latest'
    result = {'location': location, 'tides': []}
    contents = urllib2.urlopen(url).read()
    parser = TideTable()
    parser.feed(contents)
    for data in parser.data:
        date = data['date']
        times = data['time tide']
        tides = data['tide']
        level_metrics = data['level metric']
        payload = {'date': date[0], 'low_tides': [], 'high_tides': []}
        
        i = 0
        while i < len(times):
            tide = 'low_tides' if tides[i] == 'Low Tide' else 'high_tides' 
            payload[tide].append({
                'time': times[i],
                'level_metric': level_metrics[i]
            })
            i += 1
        result['tides'].append(payload)
    return result

def parse(locations):
    result = []
    for loc in locations:
        result.append(assign(loc))
    return result