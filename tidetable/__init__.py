__author__ = "Daniel Deflavia (dcd@intovpc.com)"
__version__ = "0.1dev"
__copyright__ = "Copyright (c) 2018 Daniel Deflavia"
__license__ = "Creative Commons Attribution-Noncommercial-Share Alike license"

__all__ = ['tidetable']

from .tidetable import parse